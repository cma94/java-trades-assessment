package com.citi.training.trades.dao;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.citi.training.trades.exceptions.TradeNotFoundException;
import com.citi.training.trades.model.Trade;

public class InMemTradeDaoTests {

	private static Logger LOG = LoggerFactory.getLogger(InMemTradeDaoTests.class);

	private int testId = 999;
	private String testStock = "msft";
	private double testPrice = 1.23;
	private int testVolume = 300;

	@Test
	public void test_saveAndGetTrade() {
		Trade testTrade = new Trade(-1, testStock, testPrice, testVolume);
		InMemTradeDao testRepo = new InMemTradeDao();

		testTrade = testRepo.create(testTrade);

		assertTrue(testRepo.findById(testTrade.getId()).equals(testTrade));

	}

	@Test
	public void test_saveAndGetAllTrades() {
		Trade[] testTradeArray = new Trade[100];
		InMemTradeDao testRepo = new InMemTradeDao();

		for (int i = 0; i < testTradeArray.length; ++i) {
			testTradeArray[i] = new Trade(-1, testStock, testPrice, testVolume);

			testRepo.create(testTradeArray[i]);
		}

		List<Trade> returnedTrades = testRepo.findAll();
		LOG.info("Received [" + returnedTrades.size() + "] Trades from repository");

		for (Trade thisTrade : testTradeArray) {
			assertTrue(returnedTrades.contains(thisTrade));
		}
		LOG.info("Matched [" + testTradeArray.length + "] Trades");
	}

	@Test(expected = TradeNotFoundException.class)
	public void test_deleteTrade() {
		Trade[] testTradeArray = new Trade[100];
		InMemTradeDao testRepo = new InMemTradeDao();

		for (int i = 0; i < testTradeArray.length; ++i) {
			testTradeArray[i] = new Trade(testId + i, testStock, testPrice, testVolume);

			testRepo.create(testTradeArray[i]);
		}
		Trade removedTrade = testTradeArray[5];
		testRepo.deleteById(removedTrade.getId());
		LOG.info("Removed item from Trade array");
		testRepo.findById(removedTrade.getId());
	}

	@Test(expected = TradeNotFoundException.class)
	public void test_deleteNotFoundEmployee() {
		InMemTradeDao testRepo = new InMemTradeDao();

		testRepo.create(new Trade(2, testStock, testPrice, testVolume));
		testRepo.deleteById(99);
	}
}
