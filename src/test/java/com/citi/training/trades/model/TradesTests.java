package com.citi.training.trades.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class TradesTests {

	private String testStock = "AAPL";
	private double testPrice = 1.50;
	private int testVolume = 120;

	@Test
	public void test_Trade_constructor() {
		Trade testTrade = new Trade(testStock, testPrice, testVolume);
		;

		assertEquals(testStock, testTrade.getStock());
		assertEquals(testPrice, testTrade.getPrice(), 0.0001);
		assertEquals(testVolume, testTrade.getVolume(), 0.0001);
	}

	@Test
	public void test_Trade_toString() {
		String testString = new Trade(testStock, testPrice, testVolume).toString();

		assertTrue(testString.contains(testStock));
		assertTrue(testString.contains(String.valueOf(testPrice)));
		assertTrue(testString.contains((new Integer(testVolume)).toString()));
	}
}
