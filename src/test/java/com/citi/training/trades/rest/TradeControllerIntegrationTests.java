package com.citi.training.trades.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.trades.model.Trade;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("h2")
public class TradeControllerIntegrationTests {

	private static final Logger logger = LoggerFactory.getLogger(TradeControllerIntegrationTests.class);

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void getTrade_returnsTradeEmployee() {
		restTemplate.postForEntity("/trades", new Trade(-1, "msft", 2.5, 100), Trade.class);

		ResponseEntity<List<Trade>> getAllResponse = restTemplate.exchange("/trades", HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Trade>>() {
				});

		logger.info("getAllTrade response: " + getAllResponse.getBody());

		assertEquals(HttpStatus.OK, getAllResponse.getStatusCode());
		assertTrue(getAllResponse.getBody().get(0).getStock().equals("msft"));
		assertEquals(getAllResponse.getBody().get(0).getPrice(), 2.5, 0.0001);
		assertEquals(getAllResponse.getBody().get(0).getVolume(), 100, 0.0001);

		restTemplate.delete("/employees/" + getAllResponse.getBody().get(0).getId());
	}

	@Test
	public void getTrade_returnsNotFound() {

		ResponseEntity<Trade> getByIdResponse = restTemplate.exchange("/trade/9999", HttpMethod.GET, null,
				new ParameterizedTypeReference<Trade>() {
				});

		logger.info("getByIdTrade response: " + getByIdResponse.getBody());

		assertEquals(HttpStatus.NOT_FOUND, getByIdResponse.getStatusCode());
	}
}
